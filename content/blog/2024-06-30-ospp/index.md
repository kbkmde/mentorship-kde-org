---
title: KDE participates in OSPP 2024
date: 2024-06-30
authors:
 - SPDX-License-Identifier: Burgess Chang
images:
  - images/ospp-banner.png
SPDX-License-Identifier: CC-BY-SA-4.0
---

# KDE participates in OSPP 2024

We are pleased to announce our participation in the [Open Source Promotion Plan](https://summer-ospp.ac.cn/) (OSPP) 2024. KDE will mentor a project within this program. OSPP is largely organized by The Institute of Software at the Chinese Academy of Sciences. Its goal is to encourage college students to engage in developing and maintaining open-source software. 

This marks KDE Community's first year participate in OSPP, and we mentor a project.

## Porting KDE Games to the Android Platform

KDE Games and educational games are integral parts of the KDE Gear suite. This year, under the guidance of [Benson Muite](https://invent.kde.org/kbkmde), Hanyang Zhang will undertake work in this area. [Hanyang Zhang](https://invent.kde.org/zhy) will be responsible for porting one to two KDE Games to the Android platform.

Let's warmly welcome the new contributor and wish them a enjoyable summer within KDE!
