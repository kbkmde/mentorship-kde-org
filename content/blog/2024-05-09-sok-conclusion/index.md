---
date: 2024-05-09
title: Season Of KDE 2024 Conclusion
categories: [SoK]
author: Johnny Jazeix, Joseph P. De Veaugh-Geiss
summary: Discover our successful 2024 projects
SPDX-License-Identifier: CC-BY-SA-4.0
authors:
- SPDX-FileCopyrightText: 2024 Johnny Jazeix <jazeix@gmail.com>
---

## Introduction

Another year, another successful Season Of KDE for 12 contributors!

This article has been co-written with the input from all contributors.

# Translation Projects

KDE counts on a very active translation community and translates software into
over 50 different languages. In SOK 2024, we had 2 projects that focused on
translating multiple apps into Hindi.
[Asish Kumar](https://community.kde.org/SoK/2024/StatusReport/Asish_Kumar) and
[Akash Kumar](https://community.kde.org/SoK/2024/StatusReport/Akash_Kumar)
joined the [KDE Hindi
community](https://webchat.kde.org/#/room/#kde-hindi:kde.org) to translate
multiple apps into Hindi.
They both worked together on translating
[Merkuro](https://apps.kde.org/merkuro.calendar/), then Akash focused on
[Tellico](https://apps.kde.org/tellico/) while Asish worked on [KDE
Connect](https://kdeconnect.kde.org/) and [Cantor](https://cantor.kde.org/).

![Tellico in Hindi](tellico_hindi.png)
![Cantor in Hindi](cantor_hindi.png)

# [Kdenlive](https://kdenlive.org/)

Kdenlive brings you all you need to edit and put together your own movies. We had 2 projects for KDE's full-featured video editor:

- [Ajay Chauhan](https://community.kde.org/SoK/2024/StatusReport/Ajay_Chauhan)
implemented multi-format rendering for Kdenlive by adding a filter to adjust
the aspect ratio of video clips in the main track, allowing users to select
the desired aspect ratio during export, and integrating it into the final
rendering profile.
Ajay also added code to apply filters to clips, calculate crop parameters, and
handle video cropping to the desired ratio; and implemented the GUI
component ComboBox that selects the aspect ratio and ensures that the selected
ratio is passed to the RenderRequest object. Additionally, various issues were
fixed during development, such as temporary file handling issue, preventing
crashes, and refactoring code.
![Kdenlive aspect ratio combobox](kdenlive_sok_rendering.png)

- [aisuneko icecat](https://community.kde.org/SoK/2024/StatusReport/aisuneko)
created a prototype keyframe curve editor GUI for Kdenlive. Based on recent
progress in introducing advanced keyframe types and capabilities into the
editor, the widget allows the user to intuitively view and control the current
animation curve of keyframable effect parameters. [As of
now](https://invent.kde.org/multimedia/kdenlive/-/merge_requests/478), the
widget supports basic interactions such as dragging and double clicking, and
integrates well with other existing Kdenlive components. This is
still a work-in-progress feature, as more work needs to be done beyond SoK to
have it further enhanced before it can be released to end users.
![Keyframe curve editor](keyframe-curve-editor.png)

# [KDE Eco / Accessibility](https://eco.kde.org/)

There are 5 new projects that made measuring the energy consumption of software easier and more integrated in the development pipeline. This helps make KDE software more efficient and environmentally friendly, as well as more accessible at the same time:

- [Sarthak Negi](https://community.kde.org/SoK/2024/StatusReport/Sarthak_Negi)
focused on testing, bug-fixing and integrating measurement workflows on KEcoLab.
After setting up the testing environments, Sarthak worked on creating a CI test
and refactoring code for efficiency and the code [has been
merged](https://invent.kde.org/teams/eco/remote-eco-lab/-/merge_requests/38) in
the main repository.
![Updated workflow for KEcoLab](kde_eco_pipeline.png)

- [Pradyot Ranjan](https://community.kde.org/SoK/2024/StatusReport/Prady0t)
worked on improving and updating the setup guide for
[selenium](https://www.selenium.dev/), a tool to automatize testing. The result
can be found on this [wiki page](https://community.kde.org/Selenium).
![Working with Kdenlive to make Selenium videos](kdenlive_selenium.jpeg)

- [Amartya
Chakraborty](https://community.kde.org/SoK/2024/StatusReport/Amartya_Chakraborty
) added support for KdeEcoTest on the Windows platforms.
To do this, Amartya replicated test-scripts for Okular test using KdeEcoTest
which previously used xdotool. Now this test-script can be executed on any
platform.
The conditional installation of packages based on the platform using pipenv has
been implemented.

- [Athul Raj
Kollareth](https://community.kde.org/SoK/2024/StatusReport/Athul_Raj_K) worked
on bringing support for KdeEcoTest on Wayland systems. The initial work
consisted of [adding an abstraction
layer](https://invent.kde.org/echarruau/feep-win-32-kdotool-integration/-/
merge_requests/1) so that KdeEcoTest could be run on different platforms
including Windows. To build support for Wayland, we had to first restrict our
scope and finally decided to move with only supporting the KWin compositor as it
had built in functionalities for automating window related manipulations on the
GUI. To automate input devices, the Linux kernel's evdev module was used which
allowed us to monitor input devices and also emulate them using uinput. With
these changes integrated into KdeEcoTest, we were able to run tests on Wayland,
X11 and Windows thanks to Amartya's implementation.
![KEcoTest running on Wayland](KdeEcoTest_running under wayland.png)

- [Aakarsh MJ](https://community.kde.org/SoK/2024/StatusReport/AakarshMJ) worked
on integrating KEcolab into [Okular](https://apps.kde.org/okular/)'s pipeline.
This will allow the Okular team to measure energy consumption for each
release. This paves the way for the creation of a template which will be
further helpful for other projects as well. A merge request is in progress to
integrate it into Okular.
![Local test of Okular pipeline integrating KEcolab](kde_okular_pipeline.png)

# [Cantor](https://cantor.kde.org/) / [LabPlot](https://labplot.kde.org/)

Cantor is an application that lets you use your favorite mathematical
programming language from within a friendly worksheet interface, while Labplot
is KDE's user-friendly data visualization and analysis software. Both
applications are closely intertwined, and have had three projects completed
during SOK:

- [Dhairya
Majmudar](https://community.kde.org/SoK/2024/StatusReport/Dhairya_Majmudar)
worked on extending the embedded documentation for supported Computer Algebra
Systems Project. Dhairya created the common styles for several mathematical
system documents, enhancing the users' experience allowing them to use them
simultaneously; and Python scripts have been written to link the
stylesheets to the HTML files. These Python scripts are further extended to
convert HTML files in Qt Help files that can be uploaded to the KDE Store. The
in-progress merge request can be found at:
https://invent.kde.org/education/cantor/-/merge_requests/74.
![](cantor_doc.png)

- [Israel
Galadima](https://community.kde.org/SoK/2024/StatusReport/Israel_Galadima)
contributed to the "LabPlot: Download/Import of datasets from kaggle.com"
project. Since kaggle.com seems to be the central place nowadays for finding
datasets in the data science community, we wanted LabPlot's users to be able to
access the datasets on kaggle.com directly from within LabPlot. Thus, Israel
worked on a new dialog in LabPlot that allows users to search for and import
datasets directly from kaggle.com into LabPlot spreadsheets, using the official
kaggle cli tool to facilitate the communication between LabPlot and kaggle.com.
Multiple merge requests have been merged, the [last
one](https://invent.kde.org/education/labplot/-/merge_requests/494) is still in
review.
![](labplot_kaggle.png)

- [Raphael Wirth](https://community.kde.org/SoK/2024/StatusReport/Raphael_Wirth)
introduced the support for data stored in the [MCAP](https://mcap.dev/) format
to LabPlot. Throughout the project, Raphael extended the backend of LabPlot to
allow the loading of JSON-encoded MCAP files into its internal data structure as
well as the export back to the MCAP file format. Additionally, the user
interface has been adapted to accommodate these advancements. This required the
extension of the existing import dialog and the introduction of a new export
dialog tailored specifically for saving MCAP files.
![](labplot_import_mcap.png)


We would like to congratulate all participants and look forward to their
future journey with KDE!
