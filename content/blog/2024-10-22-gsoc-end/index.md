---
description: KDE and Google Summer of Code 2024
authors:
  - SPDX-FileCopyrightText: 2024 Benson Muite <benson_muite@emailplus.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2024-10-22
hero_image: GSoCPlusKDE.png
images:
  - /announcements/2024-gsoc-end/GSoCPlusKDE.png
title: "First part of KDE and Google Summer of Code 2024"
draft: false
---

![Image of a yellow star with an orange circle centered in the star and within the star the text "</>" representing Google summer of code next to a plus sign next to a three quarter gear with a K representing KDE](https://mentorship.kde.org/blog/2024-10-22-gsoc-end/GSoCPlusKDE.png)

All but one of KDE's [Google Summer of Code](https://summerofcode.withgoogle.com/) (GSoC)
projects are complete. This post will summarize the completed project outcomes.
GSoC is a program where people who are students or are new to Free and Open Source software
make programming contributions to an open source project.

### Projects

#### [Arianna](https://apps.kde.org/arianna/)

* [Port Arianna to Foliate-js](https://community.kde.org/GSoC/2024/StatusReports/Ajay_Chauhan):
[**Ajay Chauhan**](https://invent.kde.org/intincrab) worked on porting Arianna from
[epub.js](https://github.com/futurepress/epub.js) to use
[Foliate-js](https://github.com/johnfactotum/foliate-js).
The work will hopefully be merged soon.

<figure>
  <center>
    <img src="https://mentorship.kde.org/blog/2024-10-22-gsoc-end/Arianna.png" alt="Image of a page from a book and the table of contents in Arianna"
         style="width:586px;height:420px;">
    <figcaption>
      <small>
        A screenshot of Arianna using Foliate-js to render a table of contents<br>
        (Courtesy of Ajay Chauhan, <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a>)
      </small>
    </figcaption>
  </center>
</figure>

#### [Frameworks](https://develop.kde.org/products/frameworks/)

[Python bindings for KDE Frameworks](https://community.kde.org/GSoC/2024/StatusReports/ManuelAlcaraz):

[**Manuel Alcaraz Zambrano**](https://invent.kde.org/manuelal), implemented Python bindings for
[KWidgetAddons](https://invent.kde.org/frameworks/kwidgetsaddons),
[KUnitConversion](https://invent.kde.org/frameworks/kunitconversion),
[KCoreAddons](https://invent.kde.org/frameworks/kcoreaddons),
[KGuiAddons](https://invent.kde.org/frameworks/kguiaddons),
[KI18n](https://invent.kde.org/frameworks/ki18n),
[KNotifications](https://invent.kde.org/frameworks/knotifications),
and
[KXmlGUI](https://invent.kde.org/frameworks/kxmlgui). This was done using
[Shiboken](https://doc.qt.io/qtforpython-6/shiboken6/index.html).
In addition, Manuel wrote a
[tutorial](https://alcarazzam.dev/posts/generate-python-bindings-shiboken/) on how
to generate Python bindings using Shiboken. The complicated set of merge requests
are still being reviewed, and Manuel continues to interact with the KDE community.

<figure>
  <center>
    <img src="https://mentorship.kde.org/blog/2024-10-22-gsoc-end/unit-converter.png"
         alt="Image of a widget with a box indicating length is being converted
              and further boxes for the input length and its associated unit, and
              the length converted to the specified output unit."
         style="width:568px;height:279px;">
    <figcaption>
      <small>
        Unit conversion example created using Python and KUnitConversion<br>
        (Courtesy of Manuel Alcaraz Zambrano,
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a>)
      </small>
    </figcaption>
  </center>
</figure>

#### [KDE Connect](https://kdeconnect.kde.org/)

[Update SSHD library in KDE Connect Android app](https://community.kde.org/GSoC/2024/StatusReports/ShellWen)

The main aim of [**ShellWen Chen's**](https://invent.kde.org/shellwen) project was to update
Apache Mina SSHD from 0.14.0 to 2.12.1.  The older version has a few listed
[vulnerabilities](https://mvnrepository.com/artifact/org.apache.sshd/sshd-core/0.14.0).
The newer library required additional code to enable it to work on older Android phones,
upto Android API 21.

#### [KDE Games](https://apps.kde.org/games)

[Implementing a computerized opponent for the Mancala variant Bohnenspiel](https://community.kde.org/GSoC/2024/StatusReports/JoaoGouveia):

[**João Gouveia**](https://invent.kde.org/joaotgouveia) created
[Mankala engine](https://invent.kde.org/joaotgouveia/mankalaengine), a library
to enable easy creation of Mancala games. The engine contains implementations for
two Mancala games, Bohnenspiel and Oware. Both games contain computerized opponents,
João also started on a QtQuick graphical user interface. The games are functional,
but additional investigation on computerized opponents may help improve their
effectiveness.

<figure>
  <center>
    <img src="https://mentorship.kde.org/blog/2024-10-22-gsoc-end/BohnenspielTUI.png"
         alt="Image of a grid with two rows of six holes, one row above the other.
              within each hole is the number six. Each hole also has a number next
              to it. On the left and right ends are larger holes with the number
              zero in them"
         style="width:300px;height:141px;">
    <figcaption>
      <small>
        Image of text user interface for Bohnenspiel<br>
        (Courtesy of João Gouveia,
        <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>)
      </small>
    </figcaption>
  </center>
</figure>

#### [Kdenlive](https://kdenlive.org/)

[Improved subtitling support for Kdenlive](https://community.kde.org/GSoC/2024/StatusReports/ChengkunChen):

Kdenlive has gotten improved subtitling support. [**Chengkun Chen**](https://invent.kde.org/seri)
added support for using the
[Advanced SubStation (ASS)](https://wiki.multimedia.cx/index.php?title=SubStation_Alpha) file
format and for converting [SubRip](https://wiki.multimedia.cx/index.php/SubRip) files to ASS files.
To support this format, Chengkun Chen also made subtitling editor improvements. The work has been
merged in the main repository. Documentation has been written, and will hopefully be merged soon.

<figure>
  <center>
    <img src="https://mentorship.kde.org/blog/2024-10-22-gsoc-end/kdenlive.png"
         alt="A widget with choices for font and layout of subtitle text."
         style="width:593px;height:289px;">
    <figcaption>
      <small>
        The new Style Editor Widget<br>
        (Courtesy of Chengkun Chen,
        <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>)
      </small>
    </figcaption>
  </center>
</figure>

#### [Krita](https://krita.org/)

[Creating Pixel Perfect Tool for Krita](https://community.kde.org/GSoC/2024/StatusReports/KenLo):

[**Ken Lo**](https://invent.kde.org/kenlo) worked on implementing Pixel Perfect lines in Krita. As
explained by
[Ricky Han](https://rickyhan.com/jekyll/update/2018/11/22/pixel-art-algorithm-pixel-perfect.html),
such algorithms remove corner pixels from `L` shaped blocks and ensure the thinnest possible line
is 1 pixel wide. Implementing such algorithms well is of use not only in Krita, but also in
rendering web graphics where user screen resolutions can vary significantly. The algorithm was
implemented to work in close to real time while lines are drawn, rather than as a post processing
step. Ken Lo's work has been merged into Krita.

<figure>
  <center>
    <img src="https://mentorship.kde.org/blog/2024-10-22-gsoc-end/krita.png"
         alt="Four curved lines in different colors. Three of the lines are pixel perfect
              and do not have corner block pixels, the fourth line is not quite pixel
              perfect."
         style="width:576px;height:387px;">
    <figcaption>
      <small>
        An image showing that pixel perfect lines are obtained most of the time<br>
        (Courtesy of Ken Lo,
        <a href="https://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>)
      </small>
    </figcaption>
  </center>
</figure>

#### [Labplot](https://apps.kde.org/labplot2/)

[Improve Python Interoperability with LabPlot](https://community.kde.org/GSoC/2024/StatusReports/IsraelGaladima)

[**Israel Galadima**](https://invent.kde.org/izzygala) worked on improving Python support in LabPlot.
[Shiboken](https://wiki.qt.io/Qt_for_Python/Shiboken) was used for this. 
It is now possible to call some of LabPlot functions from Python and integrate these into other
applications.

<figure>
  <center>
    <img src="https://mentorship.kde.org/blog/2024-10-22-gsoc-end/labplot.jpg"
         alt="A graph with blue dots."
         style="width:339px;height:356px;">
    <figcaption>
      <small>
        An image of a plot produced using Python bindings to Labplot<br>
        (Courtesy of Israel Galadima,
        <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>)
      </small>
    </figcaption>
  </center>
</figure>

[3D Visualization for LabPlot](https://community.kde.org/GSoC/2024/StatusReports/kuntalhere)

[**Kuntal Bar**](https://invent.kde.org/kuntalhere) added 3D graphing abilities to LabPlot.
This was done using [QtGraphs](https://doc.qt.io/qt-6/qtgraphs-index.html). The work has yet
to be merged, but there are many nice examples of 3D plots, for bar charts, scatter and
surface plots.

<figure>
  <center>
    <img src="https://mentorship.kde.org/blog/2024-10-22-gsoc-end/Bar3dLabPlot.png"
         alt="A graph with blue dots."
         style="width:586px;height:432px;">
    <figcaption>
      <small>
        A 3D bar chart<br>
        (Courtesy of Kuntal Bar, <a href="https://mit-license.org/">MIT license</a>)
      </small>
    </figcaption>
  </center>
</figure>

#### [Snaps](https://snapcraft.io/)

[Improving Snap Ecosystem in KDE](https://community.kde.org/GSoC/2024/StatusReports/soumyadghosh)

Snaps are self contained linux application packging formats.
[Soumyadeep Ghosh](https://invent.kde.org/soumyadghosh) worked
on improving the tooling necessary to make KDE applications
easily available in the [Snap Store](https://snapcraft.io/publisher/kde).
In addition, Soumyadeep improved packaging of a number of KDE
Snap packages, and packaged [MarkNote](https://snapcraft.io/marknote).
Finally, Soumyadeep created [Snap KCM](https://invent.kde.org/soumyadghosh/snap-kcm),
a graphical user interface to manage permissions that Snaps
have when running.

<figure>
  <center>
    <img src="https://mentorship.kde.org/blog/2024-10-22-gsoc-end/SnapKCM.png"
         alt="An image showing snap applications and permissions granted to one application."
         style="width:566px;height:410px;">
    <figcaption>
      <small>
        Snap KCM<br>
        (Courtesy of Soumyadeep Ghosh, <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a>)
      </small>
    </figcaption>
  </center>
</figure>

### Next Steps
The GSoC period is over, for all but one contributor,
[Pratham Gandhi](https://invent.kde.org/pgandhipro).
A follow up post will summarize contributions from the
remaining project. Contributors have enjoyed participating in
GSoC and we look forward to their continuing participation in
free and open source software communities and in contributing
to KDE. 
