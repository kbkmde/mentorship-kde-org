---
title: Get Involved
name: KDE Mentorship
userbase: KDE Mentorship
menu:
  main:
    weight: 8
header_image_link: /reusable-assets/konqi-contribute.png
---

Become a part of KDE! We need as many motivated people as possible to drive this forward. Here are some channels where you can get more information and contribute.

### Community & Support

- Matrix Room (Discussion): https://webchat.kde.org/#/room/#kde-soc:kde.org
- Mailing List (Announcements): https://mail.kde.org/cgi-bin/mailman/listinfo/kde-soc

### Resources

- GSoC (Community Wiki): https://community.kde.org/GSoC
- SoK (Community Wiki): https://community.kde.org/SoK
- Administration (Community Wiki): https://community.kde.org/SoK/Administer_the_SoK

### Contact

Email: `kde-soc [at] kde.org`
