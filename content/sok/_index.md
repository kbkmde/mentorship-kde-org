---
title: SoK
layout: SoK
menu:
#   main:
#     name: SoK
#     weight: 2
faq:
  - question: "Why Season Of KDE?"
    answer: "https://community.kde.org/SoK"
  - question: "Which project should I participate in?"
    answer: "That is up to you to decide. We want all contributors to be motivated and happy, and to continue contributing to KDE as long as they can. So it is important to choose a project that you like!"
  - question: "When should I start contributing?"
    answer: "As soon as you can! Even if the aim is to introduce as many people as we can to KDE, we unfortunately cannot mentor everyone, so there is a selection done among the different candidates. We will mostly go with the people who have contacted us to discuss their proposal and start engaging with the project."
  - question: "Where are the project ideas?"
    answer: "Go to https://community.kde.org/SoK/ and click on \"Ideas\" under \"Current year\"."
  - question: "I have an idea but it's not listed in the page. What do I do?"
    answer: "Look for a mentor by contacting the relevant people in mailing lists and KDE Matrix channels. Some KDE contributors may be happy to mentor, but they didn't have a specific idea to add to the \"Ideas\" page and so they are not listed there. Your idea will have a higher chance of attracting mentors if it aligns with one of the [KDE Goals](https://kde.org/goals)."
  - question: "Where are the SoK rules?"
    answer: "As it is a KDE internal event, the rules are not fixed and may vary each season."
---

Season of KDE is an outreach program hosted by the KDE community.

Every year since 2013, KDE has been running Season of KDE (SoK) as a program similar to, but not quite the same as [Google Summer of Code](../gsoc/index.html). SoK offers everyone an opportunity to participate in both code and non-code projects which benefit the KDE ecosystem. In the past few years, participants have not only contributed to new software features, but have also developed the KDE Continuous Integration system, created statistical reports for developers, ported KDE Applications, written documentation, and lots more.

For some inspiration for your proposals, take a look at our [Ideas](https://community.kde.org/SoK/) page for the year you would like to contribute.

{{< timeline >}}

    {{% event date="2024-12-15" title="Start of Season of KDE 2025" %}}
    {{% /event %}}

    {{% event date="2025-01-08" title="Deadline for the contributors applications"  invertedText="timeline-inverted" %}}
    {{% /event %}}
    
    {{% event date="2025-01-15" title="Projects announced" %}}
    {{% /event %}}
    
    {{% event date="2025-01-17" title="Start of work" invertedText="timeline-inverted" %}}
    {{% /event %}}
    
    {{% event date="2025-03-31" title="End of work" %}}
    {{% /event %}}
    
    {{% event date="2025-04-07" title="Results announced" invertedText="timeline-inverted" %}}
    {{% /event %}}
    
    {{% event date="2025-05-20" title="Certificates issued" %}}
    {{% /event %}}
    
    {{% event date="After the end" title="Merchandise and swag sent out by courier" invertedText="timeline-inverted" %}}
    {{% /event %}}
{{< /timeline >}}

FAQ
===
Here are the most common questions/answers for Season Of KDE:
{{< faq name="faq" >}}
